#!/bin/sh -e

VERSION=$2
TAR=../bnd_$VERSION.orig.tar.gz
DIR=bnd-$VERSION
GIT_DIR=bnd-git

DATE=2011-11-04
git clone git://github.com/bndtools/bnd.git $GIT_DIR
(
  cd $GIT_DIR
  git checkout `git rev-list -n 1 --before="$DATE" master`
)

# Bnd git cleanup
chmod +x debian/git-transform.sh
debian/git-transform.sh $GIT_DIR $DIR

tar -c -z -f $TAR $DIR
rm -rf $DIR
rm -rf $GIT_DIR

