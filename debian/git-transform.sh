#!/bin/bash
# Usage: git-transform src dest

set -e

SRC=$1
DEST=$2
SRC=${SRC:-bnd-git}
DEST=${DEST:-bnd-clean}
VERSION=$(dpkg-parsechangelog | egrep '^Version:' | cut -f 2 -d ' '| cut -d: -f2- | cut -d'-' -f 1)

rm -rf $DEST

echo "Copy files..."
rsync --cvs-exclude -r --exclude '**/*.class' --exclude '**/*.jar' --exclude '.git*' $SRC/ $DEST/

echo "Cleanup..."

# Remove all JARs
find $DEST/cnf/repo/ -name "*.jar" -delete
rm -rf $DEST/biz.aQute.bndlib/maven/org/apache
rm -rf $DEST/biz.aQute.bndlib/jar/

# Remove Eclipse compiler, not needed and under EPL-v1
rm -rf $DEST/biz.aQute.eclipse.compiler

# Remove bld (to many dependencies)
rm -rf $DEST/biz.aQute.bld

# Cleanup of binary blobs
rm -rf $DEST/biz.aQute.bndlib/test/maven-repo/
rm -rf $DEST/biz.aQute.bndlib/src/test/*.jclass
rm -rf $DEST/biz.aQute.bndlib/src/test/*.clazz
rm -rf $DEST/biz.aQute.bndlib/test/beasymock.bar
rm -rf $DEST/biz.aQute.bndlib/test/eclipse/plugins/beasymock.bar
