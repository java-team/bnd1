#!/usr/bin/make -f

include /usr/share/cdbs/1/rules/debhelper.mk
include /usr/share/cdbs/1/class/ant.mk

MAVEN_REPO           := http://repo1.maven.org/maven2/biz/aQute/
MAVEN_VERSION        := 1.50.0

PACKAGE              := $(DEB_SOURCE_PACKAGE)
VERSION              := $(DEB_UPSTREAM_VERSION)
JAVA_HOME            := /usr/lib/jvm/default-java
DEB_JARS             := ant-nodeps ant junit
DEB_ANT_BUILD_TARGET := build

### Bootstrap build
pre-build:: debian/stamp-bootstrap
bootstrap: debian/stamp-bootstrap
debian/stamp-bootstrap: DEB_BUILDDIR=$(CURDIR)
debian/stamp-bootstrap: DEB_ANT_BUILDFILE=$(CURDIR)/debian/bootstrap.xml
debian/stamp-bootstrap:
	$(DEB_ANT_INVOKE)
	mkdir -p cnf/repo/biz.aQute.bnd/ \
		cnf/repo/com.springsource.junit/ \
		cnf/repo/org.eclipse.osgi \
		cnf/repo/osgi.core \
		cnf/repo/osgi.cmpn \
		cnf/repo/ee.minimum \
		cnf/repo/junit.osgi \
		cnf/repo/org.apache.tools.ant \
		cnf/repo/org.eclipse.core.runtime \
		cnf/repo/org.eclipse.core.resources \
		cnf/repo/org.eclipse.core.commands \
		cnf/repo/org.eclipse.jdt.core \
		cnf/repo/org.eclipse.jdt.ui \
		cnf/repo/org.eclipse.jdt.debug.ui \
		cnf/repo/org.eclipse.jdt.junit \
		cnf/repo/org.eclipse.jdt.launching \
		cnf/repo/org.eclipse.jface \
		cnf/repo/org.eclipse.jface.text \
		cnf/repo/org.eclipse.swt \
		cnf/repo/org.eclipse.debug.ui \
		cnf/repo/org.eclipse.debug.core \
		cnf/repo/org.eclipse.equinox.common \
		cnf/repo/org.eclipse.equinox.registry \
		cnf/repo/org.eclipse.ui \
		cnf/repo/org.eclipse.ui.editors \
		cnf/repo/org.eclipse.ui.workbench \
		cnf/repo/org.eclipse.ui.workbench.texteditor \
		cnf/repo/org.eclipse.ui.ide \
		cnf/repo/org.eclipse.core.jobs \
		cnf/repo/org.eclipse.text \
		cnf/repo/org.osgi.impl.bundle.bindex
	ln -s $(CURDIR)/bootstrap/bnd.jar cnf/repo/biz.aQute.bnd/biz.aQute.bnd-latest.jar
	ln -s /usr/share/java/junit.jar cnf/repo/com.springsource.junit/com.springsource.junit-3.8.2.jar
	ln -s /usr/share/java/junit.jar cnf/repo/junit.osgi/junit.osgi-3.8.2.jar
	ln -s /usr/share/java/osgi.core.jar cnf/repo/org.eclipse.osgi/org.eclipse.osgi-3.5.0.jar
	ln -s /usr/share/java/osgi.core.jar cnf/repo/osgi.core/osgi.core-4.2.1.jar
	ln -s /usr/share/java/osgi.compendium.jar cnf/repo/osgi.cmpn/osgi.cmpn-4.3.0.jar
#	ln -s /usr/share/java/ee.foundation.jar cnf/repo/ee.minimum/ee.minimum-1.2.1.jar
	ln -s /usr/share/java/ant.jar cnf/repo/org.apache.tools.ant/org.apache.tools.ant-1.7.1.jar
	ln -s /usr/lib/eclipse/plugins/org.eclipse.core.runtime_*.jar cnf/repo/org.eclipse.core.runtime/org.eclipse.core.runtime-3.3.100.jar
	ln -s /usr/lib/eclipse/plugins/org.eclipse.core.resources_*.jar cnf/repo/org.eclipse.core.resources/org.eclipse.core.resources-3.3.1.jar
	ln -s /usr/lib/eclipse/plugins/org.eclipse.core.commands_*.jar cnf/repo/org.eclipse.core.commands/org.eclipse.core.commands-3.3.3.jar
	ln -s /usr/lib/eclipse/plugins/org.eclipse.jdt.core_*.jar cnf/repo/org.eclipse.jdt.core/org.eclipse.jdt.core-3.3.3.jar
	ln -s /usr/share/eclipse/dropins/jdt/plugins/org.eclipse.jdt.ui_*.jar cnf/repo/org.eclipse.jdt.ui/org.eclipse.jdt.ui-3.3.3.jar
	ln -s /usr/share/eclipse/dropins/jdt/plugins/org.eclipse.jdt.debug.ui_*.jar cnf/repo/org.eclipse.jdt.debug.ui/org.eclipse.jdt.debug.ui-3.2.102.jar
	ln -s /usr/share/eclipse/dropins/jdt/plugins/org.eclipse.jdt.junit_*.jar cnf/repo/org.eclipse.jdt.junit/org.eclipse.jdt.junit-3.3.3.jar
	ln -s /usr/share/eclipse/dropins/jdt/plugins/org.eclipse.jdt.launching_*.jar cnf/repo/org.eclipse.jdt.launching/org.eclipse.jdt.launching-3.3.3.jar
	ln -s /usr/lib/eclipse/plugins/org.eclipse.jface_*.jar cnf/repo/org.eclipse.jface/org.eclipse.jface-3.3.2.jar
	ln -s /usr/lib/eclipse/plugins/org.eclipse.jface.text_*.jar cnf/repo/org.eclipse.jface.text/org.eclipse.jface.text-3.3.2.jar
	ln -s /usr/lib/java/swt-gtk-*.jar cnf/repo/org.eclipse.swt/org.eclipse.swt-3.3.2.jar
	ln -s /usr/lib/eclipse/plugins/org.eclipse.debug.ui_*.jar cnf/repo/org.eclipse.debug.ui/org.eclipse.debug.ui-3.3.2.jar
	ln -s /usr/lib/eclipse/plugins/org.eclipse.debug.core_*.jar cnf/repo/org.eclipse.debug.core/org.eclipse.debug.core-3.3.2.jar
	ln -s /usr/lib/eclipse/plugins/org.eclipse.equinox.common_*.jar cnf/repo/org.eclipse.equinox.common/org.eclipse.equinox.common-3.3.0.jar
	ln -s /usr/lib/eclipse/plugins/org.eclipse.equinox.registry_*.jar cnf/repo/org.eclipse.equinox.registry/org.eclipse.equinox.registry-3.3.0.jar
	ln -s /usr/lib/eclipse/plugins/org.eclipse.ui_*.jar cnf/repo/org.eclipse.ui/org.eclipse.ui-3.3.2.jar
	ln -s /usr/lib/eclipse/plugins/org.eclipse.ui.editors_*.jar cnf/repo/org.eclipse.ui.editors/org.eclipse.ui.editors-3.3.2.jar
	ln -s /usr/lib/eclipse/plugins/org.eclipse.ui.workbench_*.jar cnf/repo/org.eclipse.ui.workbench/org.eclipse.ui.workbench-3.3.2.jar
	ln -s /usr/lib/eclipse/plugins/org.eclipse.ui.workbench.texteditor_*.jar cnf/repo/org.eclipse.ui.workbench.texteditor/org.eclipse.ui.workbench.texteditor-3.3.2.jar
	ln -s /usr/lib/eclipse/plugins/org.eclipse.ui.ide_*.jar cnf/repo/org.eclipse.ui.ide/org.eclipse.ui.ide-3.3.2.jar
	ln -s /usr/lib/eclipse/plugins/org.eclipse.core.jobs_*.jar cnf/repo/org.eclipse.core.jobs/org.eclipse.core.jobs-3.3.1.jar
	ln -s /usr/lib/eclipse/plugins/org.eclipse.text_*.jar cnf/repo/org.eclipse.text/org.eclipse.text-3.5.100.jar
	ln -s /usr/share/java/bindex.jar cnf/repo/org.osgi.impl.bundle.bindex/org.osgi.impl.bundle.bindex-2.2.0.jar
	touch $@
clean-bootstrap: DEB_BUILDDIR=$(CURDIR)
clean-bootstrap: DEB_ANT_BUILDFILE=$(CURDIR)/debian/bootstrap.xml
clean-bootstrap:
	$(DEB_ANT_INVOKE) clean
	rm -rf cnf/repo/biz.aQute.bnd/
	rm -f debian/stamp-bootstrap

### Standard build
install/$(PACKAGE)::
	pod2man -c 'BND for Debian GNU/Linux' \
	  -r $(PACKAGE)-$(VERSION) \
	  debian/bnd-1.5.pod > debian/bnd-1.5.1

binary-post-install/$(PACKAGE)::
	mkdir install/
	cp biz.aQute.launcher/tmp/biz.aQute.launcher.jar install/bnd.launcher.jar
	cp biz.aQute.junit/tmp/biz.aQute.junit.jar install/bnd.junit.jar
	mh_installpoms -p$(PACKAGE) -e$(VERSION)
	mh_installjar -p$(PACKAGE) -e$(VERSION) -l debian/pom-bndlib.xml     --usj-name=bndlib         biz.aQute.bndlib/tmp/biz.aQute.bndlib.jar
	mh_installjar -p$(PACKAGE) -e$(VERSION) -l debian/pom-bnd.xml        --usj-name=bnd            biz.aQute.bnd/tmp/biz.aQute.bnd.jar
	mh_installjar -p$(PACKAGE) -e$(VERSION) -l debian/pom-annotation.xml --usj-name=bnd.annotation biz.aQute.bnd/tmp/biz.aQute.bnd.annotation.jar
	jh_installlibs -p$(PACKAGE) install/bnd.launcher.jar
	jh_installlibs -p$(PACKAGE) install/bnd.junit.jar
	dh_install -p$(PACKAGE) debian/bnd.sh /usr/bin/
	mv $(DEB_DESTDIR)/usr/bin/bnd.sh $(DEB_DESTDIR)/usr/bin/bnd-1.5
	mv debian/bnd1/usr/share/java/bnd.launcher.jar debian/bnd1/usr/share/java/bnd1.launcher.jar
	mv debian/bnd1/usr/share/java/bnd.junit.jar debian/bnd1/usr/share/java/bnd1.junit.jar
	mv debian/bnd1/usr/share/java/bndlib.jar debian/bnd1/usr/share/java/bndlib1.jar
	mv debian/bnd1/usr/share/java/bnd.jar debian/bnd1/usr/share/java/bnd1.jar
	mv debian/bnd1/usr/share/java/bnd.annotation.jar debian/bnd1/usr/share/java/bnd1.annotation.jar

clean::
	-rm -f debian/stamp-bootstrap
	-find -type l -name '*.jar' -delete
	jh_clean
	mh_clean
	-rm -rf install/
	-rm -f debian/bnd1.1
	-rm -Rf bootstrap

get-orig-source:
	debian/orig-tar.sh "" $(VERSION)

get-orig-pom:
	wget --user-agent="" -O debian/pom-bnd.xml $(MAVEN_REPO)/bnd/$(MAVEN_VERSION)/bnd-$(MAVEN_VERSION).pom
	wget --user-agent="" -O debian/pom-bndlib.xml $(MAVEN_REPO)/bndlib/$(MAVEN_VERSION)/bndlib-$(MAVEN_VERSION).pom
	wget --user-agent="" -O debian/pom-annotation.xml $(MAVEN_REPO)/annotation/$(MAVEN_VERSION)/annotation-$(MAVEN_VERSION).pom

.PHONY: bootstrap clean-bootstrap
